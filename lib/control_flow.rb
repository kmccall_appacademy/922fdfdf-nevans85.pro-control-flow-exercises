# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = ''
  i = 0
  while i < str.length
    result += str[i] if str[i] == str[i].upcase
    i += 1
  end
  result
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  start_idx = str.length / 2
  grab = 1
  if str.length.even?
    start_idx -= 1
    grab += 1
  end
  str[start_idx, grab]
end

# Return the number of vowels in a string.
VOWELS = 'aeiouAEIOU'
def num_vowels(str)
  str.count(VOWELS)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  result = num
  while num > 1
    num -= 1
    result *= num
  end
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ''
  arr.each do |el|
    result += el
    result += separator unless el == arr.last
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  case_switch = true
  result = ''
  str.each_char do |chr|
    if case_switch
      result += chr.downcase
      case_switch = false
    else
      result += chr.upcase
      case_switch = true
    end
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split(' ')
  arr.each { |word| word.reverse! if word.length >= 5 }
  arr.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  results_array = []
  i = 1
  while i <= n
    if (i % 3).zero? && (i % 5).zero?
      results_array.push('fizzbuzz')
    elsif (i % 3).zero?
      results_array.push('fizz')
    elsif (i % 5).zero?
      results_array.push('buzz')
    else
      results_array.push(i)
    end
    i += 1
  end
  results_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reverse_arr = []
  i = -1
  while i >= arr.length * -1
    reverse_arr.push(arr[i])
    i -= 1
  end
  reverse_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num <= 1
  i = 2
  while i < num
    return false if (num % i).zero?
    i += 1
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  i = 1
  factors_arr = []
  while i <= num
    factors_arr.push(i) if (num % i).zero?
    i += 1
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  results_array = []
  factors(num).each do |n|
    results_array.push(n) if prime?(n)
  end
  results_array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if even_oddball?(arr)
    arr.each { |el| return el if el.even? }
  else
    arr.each { |el| return el if el.odd? }
  end
end

def even_oddball?(arr)
  odd_count = 0
  arr.each { |el| odd_count += 1 if el.odd? }
  return true if odd_count > 1
  false
end
